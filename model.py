from sklearn.linear_model import LogisticRegression
import pickle
import pymystem3
import nltk
from nltk.corpus import stopwords as nltk_stopwords
from nltk.tokenize import word_tokenize
import numpy as np
from pymystem3 import Mystem
import gensim
import re

nltk.download('punkt')
nltk.download('stopwords')

pymystem3.constants.MYSTEM_EXE = 'mystem.exe'
stopwords = set(nltk_stopwords.words('russian'))
wv = gensim.models.KeyedVectors.load_word2vec_format(open('model.bin', 'rb'), binary=True)
model = pickle.load(open('regression_word2vec_model.pkl', 'rb'))

mystem2upos = {'A': 'ADJ',
 'ADV': 'ADV',
 'ADVPRO': 'ADV',
 'ANUM': 'ADJ',
 'APRO': 'DET',
 'COM': 'ADJ',
 'CONJ': 'SCONJ',
 'INTJ': 'INTJ',
 'NONLEX': 'X',
 'NUM': 'NUM',
 'PART': 'PART',
 'PR': 'ADP',
 'S': 'NOUN',
 'SPRO': 'PRON',
 'UNKN': 'X',
 'V': 'VERB'}

def clear_text(text):
    clean_text = re.sub(r'[^а-яА-ЯёЁ]',' ',text)
    clean_text = ' '.join(clean_text.split())
    return clean_text

def delete_stopwords(text):
    word_tokens = word_tokenize(text)
    text_without_stopwords = [w for w in word_tokens if w.lower() not in stopwords]
    return ' '.join(text_without_stopwords)

def lemmatize(text_list):
    m = Mystem()
    text_list.append(' ')
    merged_text = '|'.join(text_list)

    doc = []
    res = []

    for word in m.lemmatize(merged_text):
        if '|' not in word:
            doc.append(word)
        else:
            res.append(''.join(doc))
            doc = []
    return res

def tag_mystem(text, mapping):
    m = Mystem()
    processed = m.analyze(text)
    tagged = []
    for w in processed:
        try:
            lemma = w["analysis"][0]["lex"].lower().strip()
            pos = w["analysis"][0]["gr"].split(",")[0]
            pos = pos.split("=")[0].strip()
            if mapping:
                if pos in mapping:
                    pos = mapping[pos]  # здесь мы конвертируем тэги
                else:
                    pos = "X"  # на случай, если попадется тэг, которого нет в маппинге
            tagged.append(lemma.lower() + "_" + pos)
        except:
            continue

    return tagged

def word_averaging(words, wvb):
    all_words, mean = set(), []

    for word in words:
        if isinstance(word, np.ndarray):
            mean.append(word)
        if word in list(wvb.index_to_key):
            mean.append(wvb.get_vector(word))
            all_words.add(wvb.key_to_index[word])
    if not mean:
        return np.zeros(300,)
    mean = np.array(mean).mean(axis=0)
    return mean, all_words

def result(str):
    str = delete_stopwords(lemmatize([clear_text(str)])[0])
    str_tags = tag_mystem(str, mystem2upos)
    str_vector = word_averaging(str_tags, wv)[0]
    return model.predict([str_vector])[0] > 0

if __name__ == "__main__":
    while 1:
        str = input()
        print(result(str))